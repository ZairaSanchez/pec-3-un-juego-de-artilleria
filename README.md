# PEC 3 - Un juego de plataformas modificado

## FUNCIONAMIENTO

Parte de la PEC 2:

El juego empieza con la pantalla inicial (Pantalla Principal) donde aparecen dos botones:

- "Nueva partida", que, si lo pulsamos, comenzaremos una partida, llevándonos a la pantalla del juego propiamente dicho (Nivel 1-1).
- "Salir", que cierra la aplicación.

En la segunda pantalla (Nivel 1-1), manejamos a Mario, que puede:

- Correr y saltar (con varias intensidades).
- Golpear bloques de interrogación y obtener de ellos monedas o champiñones. El champiñón rojo le hace crecer y el verde le da una vida.
- Golpear goombas por encima para matarlos.
- Morir si toca a un goomba por los lados o si cae por un abismo.
- Tocar la bandera o el asta y completar el nivel.

Cuando Mario se queda sin vidas o completa el nivel, se vuelve a la Pantalla principal.

Parte de la PEC 3:

Ahora Mario puede, además de lo anterior:

- Obtener una flor de fuego, que le hace convertirse en Mario de fuego, lo que le permite disparar bolas de fuego y matar a los goombas. Si no han golpeado a ningún goomba, las bolas se destruyen pasados tres segundos.
- Romper bloques de ladrillo si es grande o de fuego, con sistema de partículas incluido.
- Entrar en el castillo después de alcanzar el asta del final del nivel.

Uno de los goombas se mueve de manera constante hacia la izquierda, y otro se mueve entre dos tuberías.

## ESTRUCTURA

Parte de la PEC 2:

1. Creación de pantalla inicial (Pantalla Principal):
- Adición de fondo.
- Adición de música ("Tema principal").
- Creación del botón Nueva partida. 
	i. Implementación de funcionalidad (abrir pantalla Nivel 1-1 – script NuevaPartida).
- Creación del botón Salir.
	i. Implementación de funcionalidad (salir de la aplicación – script SalirJuego). 

2. Creación de pantalla de juego (Nivel 1-1).
- Adición de fondo.
- Adición de música.
	i.  Al iniciar la pantalla suena "Tema principal".
	ii. Si el jugador ha completado el nivel, suena "super_mario_bros_music_level_complete".
	iii. Si el jugador ha muerto, suena "music_new_super_mario_bros_dead".
- Creación de tilemap.
- Creación de Mario pequeño, grande, muerto y de las animaciones que conlleva correr, saltar, crecer, decrecer y morir (scripts TocaSuelo, Movimiento, SaltoMejorado, Muerte).
- Creación de goombas y de las animaciones que conlleva su movimiento y su muerte (script GolpeGoomba).
- Seguimiento de Mario por parte de la cámara (script MovimientoCamara).
- Creación de los bloques de ladrillo y de interrogación, y la funcionalidad de cuando son golpeados (script GolpeBloque).
- Creación de las monedas y de los champiñones, y de la funcionalidad de cuando son cogidos (script CogeObjeto).
- Creación de los muros invisibles que impiden salirse del juego (MuroInicial y MuroFinal), y los que permiten morir (MuroInferior) o completar el nivel (Meta) (scripts GolpeMuro, MuerteCaida y TocaBandera).
- Creación de los objetos que muestran las monedas obtenidas y las vidas.
- Creación de la lógica del juego (script ControladorJuego).

Parte de la PEC 3:

- Creación de Mario de fuego y de las animaciones que conlleva correr, saltar, convertirse en Mario de fuego o dejar de serlo, y morir.
- Creación de la flor de fuego y de la funcionalidad de cuando es cogida (script CogeObjeto).
- Creación de las bolas de fuego y su funcionalidad (scripts BolaFuego, Movimiento y GolpeGoombaBola).
- Creación de las animaciones de los goombas que les permiten moverse (scripts MovimientoGoomba y MovimientoGoombaInicialFinal).
- Creación de la funcionalidad de cuando son golpeados los bloques de ladrillo y del sistema de partículas asociado (script GolpeBloque).
- Creación de animación de Mario entrando en el castillo al completar el nivel (scripts EntraCastillo y Movimiento).
- Lógica del juego (script ControladorJuego).
